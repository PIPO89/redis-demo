FROM alpine:3.13.5

RUN apk add python3 py3-pip

COPY requirements.txt /
RUN pip3 install -r requirements.txt

RUN mkdir /app
WORKDIR /app
COPY ./redis-demo.py .

CMD ["python3", "/app/redis-demo.py"]
