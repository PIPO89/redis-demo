#!/usr/bin/python3
import redis
import time
import os

keysToInsert = os.environ['KEYS']
redisHost = os.environ['REDIS_HOST']
redisPort = os.environ['REDIS_PORT']
redisPass = os.environ['REDIS_PASS']
redisDB = os.environ['REDIS_DB']
startTime = time.time()
# Set port, db and password to convenience.
pool = redis.ConnectionPool(host=redisHost, port=redisPort, db=redisDB, password=redisPass)
r = redis.Redis(connection_pool=pool, socket_connect_timeout=900, socket_keepalive=True)
# If you want to run this locally, create an environment variable for KEYS and REDIS_HOST.

print('\n', 'Number of keys to insert: ', int(keysToInsert))
print('\n')

for index in range(0, int(keysToInsert), 1):
    pipe = r.pipeline()
    indexSerialA = f'A{index}'
    r.set(indexSerialA, 'This is a test. Konnichiwa!')
    print('Key:', index, 'Value:', r.get(indexSerialA))

    indexSerialB = f'B{index}'
    r.set(indexSerialB, 'This is a test. Konnichiwa!')
    print('Key:', index, 'Value:', r.get(indexSerialB))

    indexSerialC = f'C{index}'
    r.set(indexSerialC, 'This is a test. Konnichiwa!')
    print('Key:', index, 'Value:', r.get(indexSerialC))

    indexSerialD = f'D{index}'
    r.set(indexSerialD, 'This is a test. Konnichiwa!')
    print('Key:', index, 'Value:', r.get(indexSerialD))

    indexSerialE = f'E{index}'
    r.set(indexSerialE, 'This is a test. Konnichiwa!')
    print('Key:', index, 'Value:', r.get(indexSerialE))

    indexSerialF = f'F{index}'
    r.set(indexSerialF, 'This is a test. Konnichiwa!')
    print('Key:', index, 'Value:', r.get(indexSerialF))
    pipe.execute()

executionTime = (time.time() - startTime)
print('\n', 'Execution time in seconds: ' + str(executionTime))
